package com.example.office.dto;

import com.example.office.entity.Employee;

import java.util.Date;

public class EmployeeResponseDto {
    private int id;

    private String name;

    private String post;

    private int age;


    private String phoneNumber;

    private String address;

    private Date joinDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public EmployeeResponseDto() {
    }

    public EmployeeResponseDto(int id, String name, String post, int age, String phoneNumber, String address, Date joinDate) {
        this.id = id;
        this.name = name;
        this.post = post;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.joinDate = joinDate;
    }
    public EmployeeResponseDto(Employee employee){
        this.id = employee.getId();
        this.name= employee.getName();
        this.post = employee.getPost();
        this.age = employee.getAge();
        this.phoneNumber = employee.getPhoneNumber();
        this.address = employee.getAddress();
        this.joinDate = employee.getJoinDate();
    }

}
