package com.example.office.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class EmployeeRequestDto {
    private int id;
    @NotNull
    private String name;
    @NotNull
    private String post;
    @NotNull
    @Min(10)
    @Max(150)
    private int age;
    @NotNull
    private String phoneNumber;
    @NotNull
    private String address;

    private Date joinDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public EmployeeRequestDto() {
    }

    public EmployeeRequestDto(int id, String name, String post, int age, String phoneNumber, String address, Date joinDate) {
        this.id = id;
        this.name = name;
        this.post = post;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.joinDate = joinDate;
    }

}
