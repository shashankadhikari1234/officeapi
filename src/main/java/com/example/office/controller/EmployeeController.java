package com.example.office.controller;

import com.example.office.dto.EmployeeRequestDto;
import com.example.office.dto.EmployeeResponseDto;
import com.example.office.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @PostMapping()
    public ResponseEntity<?> addEmployee(@RequestBody @Valid EmployeeRequestDto employeeRequestDto) {
        return ResponseEntity.ok().body(employeeService.addEmployee(employeeRequestDto));
    }

    @GetMapping("/getAll")
    public List<EmployeeResponseDto> getAllEmployee() {
        return employeeService.getAllEmployee();
    }

    @PutMapping()
    public EmployeeResponseDto updateEmployee(@RequestBody @Valid EmployeeRequestDto employeeRequestDto) {
        return employeeService.updateEmployee(employeeRequestDto);

    }

    @GetMapping({"{id}"})
    public EmployeeResponseDto getEmployeeById(@PathVariable Integer id) {

        return employeeService.getEmployeeById(id);
    }

    @DeleteMapping("{id}")
    public String deleteEmployeeById(@PathVariable Integer id) {
        employeeService.deleteEmployeeById(id);
        return "delete success";
    }

}
