package com.example.office.service;

import com.example.office.dto.EmployeeRequestDto;
import com.example.office.dto.EmployeeResponseDto;
import com.example.office.entity.Employee;
import com.example.office.repo.EmployeeRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@Service
public class EmployeeServiceImpl implements EmployeeService{
    private final EmployeeRepo employeeRepo;

    public EmployeeServiceImpl(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    @Override
    public EmployeeResponseDto addEmployee(EmployeeRequestDto employeeRequestDto) {
        Employee employee = new Employee();
        employee.setName(employeeRequestDto.getName());
        employee.setAge(employeeRequestDto.getAge());
        employee.setJoinDate(new Date());
        employee.setAddress(employeeRequestDto.getAddress());
        employee.setPhoneNumber(employeeRequestDto.getPhoneNumber());
        employee.setPost(employeeRequestDto.getPost());
        employeeRepo.save(employee);
        return new EmployeeResponseDto(employee);
    }

    @Override
    public EmployeeResponseDto updateEmployee(EmployeeRequestDto employeeRequestDto) {
        Employee  employee = employeeRepo.findById(employeeRequestDto.getId()).orElseThrow(
                ()->new RuntimeException("its invlaid id")
        );
        employee.setName(employeeRequestDto.getName());
        employee.setAge(employeeRequestDto.getAge());;
        employee.setAddress(employeeRequestDto.getAddress());
        employee.setPost(employeeRequestDto.getPost());
        employee.setJoinDate(new Date());
        employee.setPhoneNumber(employeeRequestDto.getPhoneNumber());
        employeeRepo.save(employee);


        return new EmployeeResponseDto(employee) ;
    }

    @Override
    public void deleteEmployeeById(Integer id) {
        employeeRepo.findById(id).orElseThrow(
                ()->new RuntimeException("invalid id !!")
        );
        employeeRepo.deleteById(id);


    }

    @Override
    public EmployeeResponseDto getEmployeeById(Integer id) {
        Employee employee= employeeRepo.findById(id).orElseThrow(
                ()->new RuntimeException("id  not found !!")
        );

        return new EmployeeResponseDto(employee);
    }

    @Override
    public List<EmployeeResponseDto> getAllEmployee() {
        List<Employee> employee = employeeRepo.findAll();
        List<EmployeeResponseDto> employeeResponseDto = new ArrayList<>();
        for(Employee e : employee){
            employeeResponseDto.add(new EmployeeResponseDto(e));
        }
        return employeeResponseDto;
    }
}
