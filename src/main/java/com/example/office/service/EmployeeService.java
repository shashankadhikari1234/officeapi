package com.example.office.service;

import com.example.office.dto.EmployeeRequestDto;
import com.example.office.dto.EmployeeResponseDto;
import com.example.office.entity.Employee;

import java.util.List;

public interface EmployeeService {

    EmployeeResponseDto addEmployee(EmployeeRequestDto employeeRequestDto);
    EmployeeResponseDto updateEmployee(EmployeeRequestDto employeeRequestDto);
    void deleteEmployeeById(Integer id);
    EmployeeResponseDto getEmployeeById(Integer id);
    List<EmployeeResponseDto>getAllEmployee();

}
